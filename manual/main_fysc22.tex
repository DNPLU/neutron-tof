\documentclass[12pt]{article}
\title{Tagging Fast Neutrons Essentials (FYSC22)}
\input{settings}
\input{abbreviations}
\begin{document}

\AddToShipoutPicture*{\BackgroundPic}

\pagenumbering{gobble}
\sloppy
\thispagestyle{empty} 

\newpage\null
\pagebreak

\tableofcontents
\pagebreak

\printacronyms[include-classes=abbrev,name=Abbreviations]
\pagebreak

\listoffigures
\pagebreak

\pagenumbering{arabic}

\section{Overview}

The neutron tagging laboratory gives the participant an opportunity to experience how a nuclear experiment is performed on a professional level. Particular emphasis has been put on making the learning environment free and flexible, so that every student can focus on tasks close to their field of interest. Professional equipment is used to tag neutrons from an actinide/Be source, determine their energy and more.

\subsection{Learning outcome}

In accordance with the syllabus of FYSC12: Nuclear Physics and Reactors, the
goal of the laboration exercise is that the student -- after having completed the lab -- shall:

\begin{itemize}
\item be familiar with the basic properties of the neutron;
\item know the main principles of neutron detection;
\item be able to discuss the setup and procedures of the experiment and apply the knowledge in practice to measure the neutron's time-of-flight;
\item critically evaluate the experimental results and, if appropriate, hypothesize about any unexpected observation.
\end{itemize}

\subsection{General information}

\begin{itemize}
\item You have to have signed the risk assessment informing you about the safety
  rules in the lab before attending (will be done during the intro lecture).
\item This lab does \emph{not} require any written report to be handed in.
  However, coming prepared (see below) and turning in the results worked on
  during the day are mandatory. The grade for the lab will be based on the
  latter as well as the active participation during the lab.
\end{itemize}


\subsection{Prepare BEFORE the lab}

\begin{itemize}

\item You will be assigned a component of the lab, either during the
  introduction lecture \emph{or} afterwards via e-mail (through Canvas).
  \item Read the lab manual, focusing on the component you've chosen/have been given.
  \item Prepare a short presentation (around 3 minutes) aimed towards your student colleagues, explaining the component and its role in the lab.
        The presentation should be informal (i.e. no slides) but drawing schematics, equations or figures on the whiteboard is encouraged!
        Typically, you will have been a colleague that you will give a shared presentation on the same topic with.

  \textbf{Use the questions stated in appendix~\ref{questions} to guide you on what to cover.}
\end{itemize}



\subsection{Schedule}

\noindent\textbf{Morning:}
\begin{itemize}
\item Warm-up discussion with presentations and introduction to the
  experimental setup.
\item Tour of the lab and first exploration.
\end{itemize}

\noindent\textbf{Before lunch:}
\begin{itemize}
\item Open experiment session. Lots of unanswered questions from the
  morning: how reliable are the results, how can they be improved and
  what actually happens under the hood of our data acquisition system?
  Pick a topic to delve into and document your findings.
\item Calibrating the detectors and data taking.
\end{itemize}


\noindent\textbf{Lunch break}

\noindent\textbf{Afternoon:}
\begin{itemize}
\item Data analysis and determining the neutron's time-of-flight.
\item Concluding discussion. Present your findings and ask any
remaining questions you might have!
\item Work on the Jupyter Notebook and hand in a final version (see below).
\end{itemize}

\textbf{Important: }For this laboratory exercise, you \emph{do not} have to hand in a formal written report.
Instead you will summarize what you have learned together with the data analysis in a pre-prepared Jupyter Notebook.
Therefore, your \emph{active} participation in the laboratory experiment is the key prerequisite for a passing grade: by generally asking questions, raising ideas for investigations, and by discussing the experiment and its result within the group.
And of course, by following your own initiative when investigating a subject of your choice and by presenting your knowledge to the group during the introduction session as well as the final discussion.

Please, do not forget to leave your feedback with the supervisor -- it is greatly appreciate in the continued development of this exercise!

\input{credits}
\input{experiment}
\input{theory}
\input{equipment}

\printbibliography
\pagebreak


\begin{appendices}

\appendix{}

\section{Suggested questions to investigate}
\label{questions}

For each of the topics below, we give a few questions to guide you when
preparing for the lab and as reference for the introduction discussion with your peers.

\begin{itemize}[leftmargin=6em]
\item[\textbf{Source}] We are using an ``Am/Be'' as source of free neutrons
  \begin{itemize}
  \item What is Americium and how does it decay?
  \item How does the interaction with the Beryllium lead to free neutrons?
  \item What are the typical energies involved?
  \item How is the source packaged physically?
  \end{itemize}
\item[\textbf{Scintillator}] We are using ``YAP'' and ``NE-213'' scintillators
  \begin{itemize}
  \item What is a scintillator?
  \item What are these materials?
  \item How do they interact with different types of radiation relevant in this
    experiment?
  \item What are characteristics of these materials that are important to us?
  \end{itemize}
\item[\textbf{PMT}] We are using different photomultipliers
  \begin{itemize}
  \item What is the purpose of a PMT?
  \item How does it work and what are the underlying physical processes?
  \item How does it look internally?
  \item How do you operate it?
  \end{itemize}
\item[\textbf{Electronics}] We are using a few different electronics modules\footnote{If this is the first time you encounter these type of
    equipment, it might seem intimidating -- just focus on the questions stated
    and answer them best you can!}
  \begin{itemize}
  \item What is a ``FI/FO'' and what does it do?
  \item What is a ``CFD'' and how does it work?
  \item What is a ``TDC'' and what does it do?
    \item In what order do we connect these modules in our experiment?
  \end{itemize}
\item[\textbf{ToF}] We want to measure the Time-of-Flight of the neutron
  \begin{itemize}
  \item Do we actually measure the ToF? If not, what do we determine?
  \item How to we calibrate the time to compensate for delays in cables and
    electronics?
  \item What does the ToF spectrum look like?
  \item How do you get the energy of the neutron from the ToF?
  \end{itemize}
\end{itemize}



\input{safety}

\input{analysis}

\end{appendices}

\input{feedback}


\end{document}

%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-command-extra-options: "-shell-escape"
%%% End:
