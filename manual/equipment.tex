\section{Equipment}
\label{Equipment}

\subsection{Detectors}

\subsubsection{\ac{YAP} detectors}

\begin{wrapfigure}{L}{0.5\textwidth}
\centering
\includegraphics[width=8cm]{YAP}
\caption{\ac{YAP} detector with a 10cm long PMT}
\label{YAP}
\end{wrapfigure}

A \ac{YAP} detector consists of a cylindrical yttrium aluminum perovskite crystal, $\SI{1}{in}$ high and $\SI{1}{in}$ in diameter, coupled to a photomultiplier tube (\ac{PMT}), see fig.\ref{YAP}. The detector is operated at about $\SI{800}{V}$. These detectors are quite insensitive to neutrons of any energy, which makes them ideal for detecting $\gamma$-rays within the large fast-neutron field of an actinide/Be source. Because of their small size, they are not used for spectroscopy, but simply to trigger on any portion of energy deposited by the $\SI{4.44}{MeV}$ $\gamma$-rays coming from the source.


\subsubsection{\ac{NE-213} liquid scintillator detector}

\begin{wrapfigure}{L}{0.5\textwidth}
\centering
\includegraphics[width=7.9cm, height=7cm]{NE-213}
\caption{\ac{NE-213} detector}
\label{NE-213}
\end{wrapfigure}

The \ac{NE-213} fast-neutron and $\gamma$-ray detector employed in this lab is shown in Fig. \ref{NE-213}. A $\SI{3}{mm}$ thick cylindrical aluminum cell with a depth of $\SI{62}{mm}$ and a diameter of $\SI{94}{mm}$ houses the liquid \ac{NE-213}. The inside of the cell is coated with a reflective paint, the cell is sealed with a glass plate. The assembled cell is filled with the nitrogen-flushed \ac{NE-213}. The glass window of the cell is coupled to a cylindrical \ac{PMMA} \ac{UVT} lightguide. The lightguide is then pressure-coupled to a spring-loaded, magnetically shielded \ac{PMT} assembly operated at about $\SI{-2000}{V}$. Gain for the \ac{NE-213} detector is set using an \ac{NE-213} detector event trigger and a set of standard $\gamma$-ray sources together with the prescription of Knox and Miller (1972).

\pagebreak

\subsubsection{\ac{PMT}}

\begin{wrapfigure}[20]{r}{0.45\textwidth}
\centering
\includegraphics[width=8cm]{PMT}
\caption{A schematic drawing of a Photo-Multiplier Tube\cite{pmt}}
\label{PMT}
\end{wrapfigure}

When scintillators were first used in 1903, the light emitted was registered by eye. In 1944, the Photo-Multiplier Tube (\ac{PMT}) was invented. Today, \ac{PMT}s replace our eyes. A scintillator coupled to a \ac{PMT} is shown in Fig.~\ref{PMT}. The photons created in the scintillator by the incident radiation pass into the PMT through an optical window which is followed by a photocathode. At the photocathode, the scintillation photons free electrons via the photoelectric effect. These freed photoelectrons are focused by an electrode and, using an electric field, accelerated towards a series of dynodes. At each dynode the incident electrons are multiplied, so that a single photoelectron from the photocathode can create a flow millions of electrons to the final dynode, which is called the ``anode". The electrical signal produced by the PMT is very fast ($\SI{}{ns}$ rise-time) and is proportional to the energy of the incoming radiation.


\subsection{Electronics and Data-Acquisition}

In order to determine the time-of-flight of the neutron emitted from the source,
both the charge and the timing of the signals coming from the detectors have to be determined and recorded
for each event (neutron). In this experiment, fast analog electronics are employed. There exists a variety of
electronics modules that specialise in doing things like making copies of signals, checking if signals are above a certain
value, check at which time a signal arrives and more. For this experiment, a combination of modules are
used as shown in Figure~\ref{fig:tof_electronics_setup}. The resulting data is read-out using a PC-based software
and stored on disk for later analysis. The individual components are explained in the sections below.

\begin{figure}[htbp]
\centering
\includegraphics[width=10cm]{figure_05_setup}
\caption{A simplified overview of the tagged-neutron setup. The Am/Be source, a single YAP detector (for gammas),
and the NE-213 detector (for neutrons) are all shown together with a block electronics diagram.}
\label{fig:tof_electronics_setup}
\end{figure}

\subsubsection{Analog Fan-in/Fan-out (\ac{FIFO})}

Analog Fan-in/Fan-out (\ac{FIFO}) is used to take a number of incoming signals, adding them together and producing multiple copies of the
summed signal. By using only one input signal, FI/FO-modules can be used as a simple signal duplicator. A FI/FO-module typically handles
multiple sets of input signals, with multiple corresponding sets of output signals. In this experiment, a FI/FO is used to
duplicate the signals from the detectors so that one copy can be used to start and stop the timer (TDC) and another copy can be saved for other uses,
such as integrating the charge of the signal in a QDC.

\subsubsection{Constant-fraction or timing discriminator (\ac{CFD})}

\begin{figure}
\centering
\includegraphics[width=14cm]{CFDpic}
\caption{The workings of a \ac{CFD}. The initial black signal is split. The green portion is inverted, delayed and then summed with the blue signal
to become the rightmost red signal. The zero-crossing point may be used for walk-free timing.\cite{Rofors}}
\label{CFDpic}
\end{figure}

In cases where the slow-rising pulses of a scintillator are of similar
order-of-magnitude as the time resolution needed for the experiment,
time-critical signals such as a trigger decision have to be handled very
carefully. If a certain threshold value is used for triggering and the rise-time
for all signals to reach their respective maximum is identical, then the
threshold-crossing time depends on the signal's pulse height. This unwanted effect is
known as ``time walk'' of the trigger.

In a \ac{CFD} the analog input signal is first split. One part is then inverted and the other is delayed. Then
the signals are unified again and the resulting new signal has a zero-crossing point that can be used for timing.
This zero-crossing point will not move, or ``walk'', in time if the amplitude of the input signal changes. By triggering on the zero-crossing point rather
than the signal height, a walk-free timing spectrum is achieved (see
Fig.~\ref{CFDpic}). The output of a CFD is a logic pulse with very short rise time. Using this technique, one can create a
trigger signal with reliable timing and high resolution.

\subsubsection{Time-to-digital converter (\ac{TDC})}

A \ac{TDC} is a timing device that can measure times of and between incoming signals with high accuracy and deliver the measured
time as a digital signal. From Wikipedia\cite{TDC}:A \ac{TDC} is a timing device for recognizing events and providing a digital representation of the time of their occurrence.
A \ac{TDC} can output the time between two events, or the time of arrival for each incoming pulse, measuring a time interval or
absolute time. The result is then converted into a digital (binary) output signal. A measurement is started and stopped, when
either the rising or the falling edge of a signal pulse crosses a set threshold. In atomic and high energy physics, a \ac{TDC}
is used in such experiments as time-of-flight measurement and lifetime determination.

\subsection{Data acquisition system software}

To read out the data acquired with the system, a DAQ software running on a Linux PC is used. The VME crate is connected to the
PC by a \ac{PCI}-VME bus adapter providing a link to the electronics. The DAQ software is written in C++ and based
on the data-analysis framework ROOT \cite{ROOT} to provide online data
monitoring and data storage. Signals are recorded on an event-by-event basis for
offline analysis in the binary ROOT file format.
