\section{Theory}
\label{Theory}

\subsection{The neutron}

\begin{wrapfigure}{L}{0.38\textwidth}
\centering
\includegraphics[width=4.2cm, height=4.2cm]
{Quark_structure_neutron}
\caption{Quark structure of a neutron \cite{Quark_structure_of_a_neutron}.}
\label{Quark structure of a neutron}
\end{wrapfigure}

A neutron is a subatomic particle with no electric charge and a mass of $\SI {1.009}{u}$ (atomic mass units\footnote{The unified atomic mass unit ($\SI{}{u}$) is the standard unit used to indicate the mass on the atomic scale. $\SI{1}{u}=1.66053904 \times 10^{-27}\SI{}{kg}$.}), which is slightly heavier than a proton. Together, protons and neutrons form the nucleus and are bound by the strong force that attracts all nucleons separated by about $\SI{1}{fm}$. A neutron is formed from quarks, also held together by the strong force (shown in the Fig. \ref{Quark structure of a neutron}).  The strong force is approximately 137 times stronger than the Coulomb force and much stronger than any other fundamental force. Most of the mass of a neutron or a proton comes from the strong force field energy, the mass of the quarks themselves ads up to only $\SI{1}{\%}$ of the total mass.\\

\begin{wrapfigure}{R}{0.52\textwidth}
\centering
\includegraphics[width=7.7cm, height=9cm]
{stability}
\caption{Graph of isotope stability \cite{stability}.}
\label{Graph of isotope stability}
\end{wrapfigure}

 The balance of the attractive strong force and the repulsive Coulomb force between all nucleons determines if a nucleus is stable or not. The presence of neutrons is thus key to the stability of nuclei. Fig. \ref{Graph of isotope stability} shows isotope stability and what decay processes the unstable isotopes undergo. It is evident that all but the lightest of stable nuclei have more neutrons than protons.


In 2007, a study \cite{2007PhRvL..99k2001M} of the neutron charge densities showed that, in a simplified view, the neutron has a negatively charged outer layer, a positively charged middle layer, and a negatively charged core. The negatively charged outer shell, having a larger radius, contributes more to the magnetic moment. Therefore, the magnetic moment of the neutron resembles that of a negatively charged particle. The negatively charged outer layer also assists proton-neutron bonding in a nucleus.
\pagebreak

\subsection{Discovery of the neutron}

Historically, a chain of crucial revelations led to the discovery of the neutron. The focus of physics turned to the atom at the start of the 20th century. The 1911 Rutherford model described the atom as having a heavy but small positively charged nucleus surrounded by a much larger negatively charged electron cloud. Stability was a problem for this model.

\begin{wrapfigure}{L}{0.28\textwidth}
\centering
\includegraphics[height=5cm, width=4cm]{Rutherford}
\label{Rutherford}
\caption{Rutherford \cite{Rutherford}.}
\end{wrapfigure}

In 1920, Rutherford introduced a new model to account for mass and charge differences. The nucleus was now seen as consisting of protons and ``neutral" particles thought to be made of a proton and an electron bound together. For 10 years, this was the prevailing model of the nucleus. Electrons in the nucleus were justified because it was known that beta radiation was emitted from the nucleus. However, Heisenberg's Uncertainty Principle and inconsistencies regarding particle spins still presented a problem. In 1931, a deeply penetrating radiation was observed when beryllium, boron, or lithium was bombarded with $\alpha$-particles. This was initially thought to be $\gamma$-radiation, but left some scientists doubtful. In 1932, James Chadwick performed a series of experiments to determine the nature of this radiation and concluded that it must consist of uncharged particles having masses essentially equal to that of the proton. These particles were neutrons. In 1935, Chadwick won the Nobel Prize for his work. The discovery of neutrons was a big step forwards for nuclear physics. It opened doors for new experiments and never-before-seen radioactive elements were found. Eventually, nuclear fission was discovered leading to the development of the atom bomb.

\subsection{Sources of free neutrons}

While bund, neutrons are stable. Free neutrons are unstable, with a half life of about 15 minutes, they can be produced in a multitude of ways.\\

\begin{wrapfigure}{R}{0.55\textwidth}
\centering
\includegraphics[width=7.5cm, height=3.9cm]{Cosmic_rays}
\caption{Cosmic rays \cite{cosmic_rays}.}
\label{Cosmic rays}
\end{wrapfigure}

Cosmic rays (illustrated in Fig. \ref{Cosmic rays}) interacting with the atmosphere produce high-energy muons. The muons are energetic enough to not only travel deep into atmosphere, but are even capable of reaching the surface of the Earth. When these muons strike other particles, a neutron may be released in a spallation reaction. A natural neutron background due to cosmic rays exists everywhere on Earth \cite{JGRD:JGRD51947}.\\

\begin{wrapfigure}{R}{0.55\textwidth}
\centering
\includegraphics[width=7.5cm, height=3.8cm]{accelerator}
\caption{The Large Hadron Collider\cite {accelerator}.}
\label{accelerator}
\includegraphics[width=7.5cm, height=3.8cm]{reactor}
\caption{The Watts Bar Nuclear Plant \cite{reactor}.}
\label{reactor}
\end{wrapfigure}

Particle accelerators (like the famous LHC in Fig. \ref{accelerator}) with hydrogen, deuterium, or tritium ion sources may also be used to produce neutrons using targets of deuterium, tritium, lithium, beryllium, and other low-Z materials. Typically, these accelerators operate with energies of $\SI{}{GeV}$ or higher \cite {neutron_generators}. Spallation sources use multi-$\SI{}{GeV}$ protons together with high-Z targets to produce even more energetic neutrons \cite{Taylor1092}.

Certain radioactive isotopes undergo spontaneous fission which results in the emission of neutrons. All spontaneous-fission neutron sources are produced by irradiating uranium or another transuranic element in a nuclear reactor (see Fig. \ref{reactor}).

Neutrons may also be produced by bombarding one of several low atomic weight isotopes, like beryllium, carbon or oxygen, with $\alpha$-particles. This nuclear reaction can be used to construct a radioactive neutron source by intermixing a radioisotope that emits $\alpha$-particles with a low atomic weight isotope. Irradiating a radioisotope with $\gamma$-rays that exceed the neutron binding energy of a nucleus can also result in the emission of a neutron.

\subsubsection{Actinide/Beryllium sources}


\begin{wrapfigure}{L}{0.55\textwidth}
\centering
\includegraphics[width=9.2cm]{actinides}
\caption{Actinides\cite{actinides}}
\label{actinides}
\end{wrapfigure}

All but one of the actinides are so called ``f-block" elements, corresponding to the filling of the 5f electron shell.  They have very large atomic and ionic radii and exhibit a wide range of physical properties. All actinides are radioactive and release energy upon decay. Many are synthetic elements \cite{chemistry}. Nuclear-weapons tests have released at least six actinides heavier than plutonium into the environment. As early as 1952, analysis of debris from hydrogen-bomb detonations showed the presence of americium, curium, berkelium, californium, einsteinium, and fermium \cite{bomb}.


\begin{wrapfigure}[22]{r}{0.66\textwidth}
\centering
\includegraphics[width=9.5cm]{Neutron_energy}
\caption{Relative probabilities for the state of the recoiling $^{12}\rm C$ in an $\alpha / \rm Be$ reaction \cite{Vijaya_and_Kumar}.}
\label{Carbon levels}
\end{wrapfigure}


Beryllium-based neutron sources exploit a particular nuclear reaction involving an $\alpha$-particle. The $\alpha$-particle enters a natural beryllium nucleus and is captured, resulting in a $^{12}\rm C$ nucleus and a free neutron:

\begin{equation}
\alpha + ^{9}\textrm{Be} \rightarrow ^{12}\textrm{C} + n + Q.
\end{equation}

\noindent $Q$ represents the energy difference of initial and final states.

The recoiling carbon nucleus can be produced in the ground state, the first-, or the second-excited state. The calculations of Vijaya and Kumar \cite{Vijaya_and_Kumar} suggest that the relative populations of the ground/first/second excited states for the recoiling $^{12}\rm C$ nucleus are $\sim35\%/\sim55\%/\sim15\%$ (see Fig. \ref{Carbon levels}). If the recoiling $^{12}\rm C$ nucleus is left in its first-excited state, it will promptly decay to the ground state via the isotropic emission of a $\SI{4.44}{MeV}$ $\gamma$-ray. Mowlavi and Koohi-Fayegh \cite{Mowlavi} as well as Liu et al. \cite{Liu} have measured $R$ -- the $4.44$ MeV $\gamma$-ray to neutron ratio -- for an actinide/Be source to be approximately $0.58$.

The $\alpha$-particle can come from any number of radioactive $\alpha$-emitters, or from a particle accelerator that accelerates helium ions.  Typical radioactive emitters historically used in neutron sources are $^{222}\rm Rn$, $^{226}\rm Ra$, $^{210}\rm Po$, $^{239}\rm Pu$ and $^{241}\rm Am$. Properties such as  yield, lifetime, spectrum, and also price, depend on the $\alpha$-particle source. For example, $\rm Po/Be$ $\alpha$-emitters have a high yield, but due to the short half-life of $^{210}\rm Po$ -- only 138.2 days, it is necessary to make time-dependent corrections for the yield, and the source rather quickly becomes too weak to use. Chemically, these sources are mixtures, therefore the spectrum varies with grain size of the powders which are combined. Even after the source is fabricated, the extent of mixing can change (for example, due to heating). $\rm Pu/Be$, on the other hand, is an intermetallic compound, and the spectrum consequently does not depend upon grain size. In addition, its relatively long half-life ($2.2 \times 10^4$ years), assures constancy within a life time.

\subsubsection{Am/Be source}

\begin{wrapfigure}{L}{0.3\textwidth}
\centering
\includegraphics[width=4cm]{AmBe_source}
\caption{``X.3" capsule.}
\label{X.3 capsule}
\end{wrapfigure}

$\rm Am/Be$ radioactive source is a mixture of americium oxide and beryllium. Radioactive $^{241}\rm Am$ is a long-lived alpha emitter that has a half-life of 432.2 years\footnote{The ground state of the daughter nucleus, $^{237}\rm Np$, is also unstable, but has a very long half-life of over 2 million years.}. Many of the properties of $\rm Am/Be$ make it suitable for \ac{TOF}. This laboratory uses a (nominal) $\SI{18.5}{GBq}$ $\rm Am/Be$ source contained in an ``X.3" capsule, see Fig. $\ref{X.3 capsule}$. The $^{241}\rm Am$ alpha decay is:

\begin{equation}
^{241}\textrm{Am} \rightarrow ^{237}\textrm{Np} + \alpha + Q_{\alpha}
\end{equation}
\\

\noindent The $Q_{\alpha}$-value (in MeV) of this process \cite{radioactivity} is:

\begin{equation}
\begin{split}
Q_{\alpha}=(M_{^{241}\textrm{Am}} - (M_{^{237}\textrm{Np}}+M_{\alpha}))(\SI{931.494}{MeV/u})\\
=(\SI{241.056822}{u}-(\SI{237.048166}{u} + \SI{4.00260325}{u}))(\SI{931.494}{MeV/u})\\
=\SI{5.638}{MeV},
\end{split}
\end{equation}\\

\noindent where $M$ is the mass of the isotope in question.\\

Fast neutrons are produced when the decay $\alpha$-particles subsequently interact with $^9\rm Be$:

\begin{equation}
\alpha + ^{9}\textrm{Be} \rightarrow n + ^{12}\textrm{C} + Q.
\end{equation}

\noindent Depending on the interaction and its kinematics, $^{12}\rm C$ and
a free neutron may be produced. The resulting free-neutron distribution
has a maximum value of about 11 MeV and a substructure
of peaks, whose energies and relative intensities vary
depending upon the properties of the $\rm Am/Be$ source containment
capsule and the size of the $^{241}\rm AmO_2$ and $\rm Be$ particles in the powders employed. In general, approximately $25\%$ of the neutrons emitted have an energy of less than $\sim1$ MeV with a mean energy of
$\sim400$ keV \cite{Vijaya_and_Kumar}. The average fast neutron energy is $\sim4.5$ MeV \cite{Vijaya_and_Kumar}. The unshielded source used in this lab has been independently determined to emit $(1.106 \pm 0.015)\times 10^6$ neutrons per second nearly isotropically. The kinematics of the $^9\textrm{Be}(\alpha, n)$ interaction determine the state of the recoiling $^{12}\rm C$ nucleus produced in the reaction. As already mentioned, almost $60\%$ of the neutrons emitted by an $\rm Am/Be$ source are accompanied by a prompt, time-correlated $4.44$ MeV $\gamma$-ray.
\pagebreak

\subsection{Neutron-matter interaction}

\begin{wrapfigure}{L}{0.58\textwidth}
\includegraphics[width=9.5cm, height=3.1cm]{neutron_reactions}
\caption{Types of neutron-matter interaction.\cite{neutron_reactions}}
\label{neutron_reaction}

\end{wrapfigure}

Since a neutron is a neutral particle, neither the electrons surrounding a nucleus nor the nucleus itself can affect it's path through Coulomb interaction. Thus, neutrons move in straight lines until they collide with a nucleus and are absorbed or scattered in a different direction. The possible neutron reactions are shown in the diagram on the left.

\subsubsection{Scattering reactions}

A neutron scattering reaction always results in the emission of a single neutron after the incident neutron has interacted with a nucleus. If energy is not transferred into nuclear excitation, it is an elastic scattering. In an inelastic scattering some energy of the incident neutron is absorbed by the recoiling nucleus and the nucleus is left in an excited state.

\subsubsection{Neutron absorption}

An absorbtion reaction occurs if the incident neutron is absorbed by a nucleus,  forming a compound nucleus. The type of decay, that the daughter nucleus undergoes later does not depend on how the nucleus was formed. Thus, a variety of emissions or decays may follow. The probability of absorption is proportional to the time the neutron is in the vicinity of the nucleus, which in turn is inversely proportional to the relative velocity between the neutron and the target nucleus.

Most absorption reactions result in the loss of a neutron coupled with the production of one or more $\gamma$-rays. This is known as a capture reaction (it is the only possible absorbtion reaction for non-fissionable nuclei). Radiative capture can occur at all incident neutron energies, but the likelihood of that depends on both the energy of the neutron and the temperature (also energy) of the target.

In nuclear fission the nucleus splits into smaller parts - lighter nuclei, often accompanied by free neutrons, photons, and $\gamma$-rays. A large amount of energy is released. When nuclear fission is triggered by an incident neutron, it is regarded as a nuclear reaction. It is a radioactive decay process referred to as spontaneous fission. This process occurs rarely.

\subsubsection{Transfer reactions}

In a transfer reaction a compound nucleus is formed in an excited state and ejects either a charged particle or an extra neutron. The reaction is more common for charged incident particles, however, they can play a role in neutron detection.
\pagebreak

\subsection{Neutron detection}

Neutrons have no net charge, thus, they do not ionize the material with which they interact directly. However, high-energy neutrons can be scattered by target isotopes. The recoiling target nucleons can then cause ionization if they are charged, and have enough energy. Ionizing radiation, in turn, produces light in a process known as scintillation. The detection of ionizing radiation using scintillation light is one of the oldest and most useful techniques. It is also the basis for well approved neutron detection methods.\\

\subsubsection{Scintillation}

Scintillation light is a form of luminescence that results from the de-excitation of electron states previously excited by the interaction with ionizing radiation. Sometimes, the excited state is metastable, so the relaxation back down from the excited state to lower states is delayed. The delayed light provides a way to discriminate between different ionizing incident particles. Scintillators can be in any state of matter and the fundamental interactions leading to scintillation light are diverse. It is often convenient to divide the materials into organic scintillators, inorganic scintillators, and noble gas scintillators.

\subsubsection{Organic scintillators}

Organic scintillators consist of aromatic organic molecules. Electrons of some
orbitals (called $\pi$-electrons) are only weakly bound and the luminescence is caused by transitions from the excited states of these $\pi$-orbitals to the ground state.

\begin{wrapfigure}{L}{0.63\textwidth}
\centering
\includegraphics[width=10cm]{pi_structure}
\caption{Energy levels of an organic molecule with $\pi$-electron structure\cite{pi_structure}.}
\label{Energy levels}
\end{wrapfigure}

The ionizing radiation can excite the organic molecule of a scintillator into any of a series of singlet and triplet states that are further divided into various vibrational states. Depending on the time scale of the de-excitation, it is common to divide the emitted scintillation light into two groups: fluorescent light for fast decays ($\sim \SI{1}{ns}$) and phosphorescent light for slow decays ($\sim \SI{1}{\mu s}$). Both time scales can be correlates to different electron excitation-states, singlet or triplet. Fluorescence corresponds to photo-emissions from de-excitation of an excited singlet state to a lower state of the same spin multiplicity. Singlet-singlet transitions are quantum mechanically favored and are therefore fast. Phosphorescence corresponds to photo-emissions from the de-excitation of an excited triplet state to a lower singlet state. Triplet-singlet transitions are quantum mechanically forbidden and the decays are thus delayed (see figure \ref{Energy levels}).

The organic molecules are naturally in the ground state, because the spacing between the vibrational states is larger than the average thermal energy. Any electron in the higher excited singlet states is quickly de-excited to the $\rm S_1$ state without emitting light. Molecules in the $\rm S_1$ state then transition to one of the vibrational states of the ground state emitting light. This is known as the prompt fluorescence. Molecules in the $\rm T_1$ state need to interact with other molecules in the same state, via inter-molecular interaction. Two product molecules can be formed, one in the $\rm S_1$ state, the other in the ground state. The molecule in the $\rm S_1$ state then de-excites also emitting light. This is known as the delayed fluorescence. The density of triplet states directly affects the relation between the prompt and the delayed fluorescence. The faster the incident particle loses energy in the scintillator, the more triplet states are inhibited, the greater the fraction of scintillation light due to delayed fluorescence.

\subsubsection{Inorganic scintillators}

Scintillation light production in inorganic scintillators depends on the energy states of the crystal lattices of the material. In crystals, electrons have only discrete energy levels available which form clearly separated energy bands. Electrons in the valence band, the lower energetic band, are strongly bound at the lattice site. Electrons in the conduction band have sufficient energy to freely migrate through the crystal. In the band gap of a pure crystal, the energy levels between the valence and the conduction band, no electrons are found. If energy is absorbed by the crystal, an electron from the valence band can be excited to the conduction band.

The hole left by such a process can then be filled again if the electron returns to the valence band by emitting light. The excited states have a relatively long life time ($\sim \SI{100}{ms}$). The band gap in a typical inorganic scintillator is large and the emitted light is outside the visible light range.

To increase the amount of visible scintillation light, small amounts of impurities are added to inorganic scintillators. These are called the activators, they are chosen to have energy states in the otherwise forbidden band gap. Electrons can de-excite using activator provided energy levels in a two-step process. The light emitted by these transitions is shifted to lower energies\cite{knoll}.

\subsubsection{Light yield and scintillation efficiency}

\begin{wrapfigure}{L}{0.6\textwidth}
\centering
\includegraphics[width=10cm]{light_yield}
\caption{The light yield of \ac{EJ-301} for electrons, protons, $\alpha$-particles and $^{12}C$, showing the quenching effect and non-linear light production\cite{light_yield}.}
\label{light_yield}
\end{wrapfigure}

Scintillation in most materials is an inefficient process and scintillators convert only a small fraction of the energy deposited by ionizing radiation into scintillation light. Most of the energy is transfered to heat and vibrational excitation and is not available for light production. The scintillation efficiency depends on the type of ionizing radiation. The highly ionizing protons and heavy ions causes damage to molecules along their path and, as a result, less light is produced. This is known as quenching, and the effect is proportional to the ionization density of the incident radiation. In general, scintillation efficiency is also dependent on the energy of the incident particle. However, for all types of radiation in noble-gas scintillators and for electrons\footnote{If the electron energy is above $\SI{100}{keV}$.} in both inorganic and organic scintillator materials, the efficiency is independent of energy. The scintillation light yield per unit path length $\frac{dL}{dx}$\cite{pi_structure,knoll} to a good approximation is:

\begin{equation}
\frac{dL}{dx}=\frac{S \frac{dE}{dx}}{1+kB\frac{dE}{dx}},
\end{equation}

\noindent where $\frac{dE}{dx}$ is the energy deposition per unit path length, $B$ is the proportionality constant for the damaged molecule density, $k$ is the fraction of damaged molecules in which quenching occurs. The light yield dependence on radiation type and energy for a liquid scintillator material is shown in figure \ref{light_yield}.

\subsubsection{Fast-neutron detection with scintillator detectors}

At the $\SI{1}{MeV}$ - $\SI{10}{MeV}$ energy range neutrons mostly interact via scattering. This type of reaction is also prominent in the energy transfer from a neutron to a particle that can ionize a scintillator material. The process of scintillation can be used for indirect neutron detection.

The neutron scatter reaction cross-section varies from one nucleus to another. Even more importantly, the maximum energy transfer possible from a neutron to the recoiling nucleus in one elastic scatter event is dependent on the mass relation of the interacting particles, lighter nuclei, like a hydrogen nucleus (with maximum energy transfer of nearly $100\%$), deuterium, helium, or other, are therefore preferred. Organic scintillators consisting of mainly hydrogen and carbon atoms are a natural choice for detecting neutrons. Although neutrons interact with the nuclei of both atoms, and the reaction cross-sections are comparable, next to no light is produced by recoiling carbon nuclei due to large quenching effects. If a scintillator consists from mainly hydrogen and carbon without many other elements in it, and the scintillation light comes mainly from the recoiling hydrogen nuclei, the scintillation light spectrum is reasonably easy to interpret. However, the scintillation efficiency still depends on all the elements and the respective neutron cross-sections.

\pagebreak
