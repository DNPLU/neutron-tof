\pagebreak

\section{Experiment}
\label{Experiment}

The time-of-flight (\ac{TOF}) technique is used to measure and study the energy spectrum of fast neutrons produced by a neutron source. First, the distribution in time of the neutrons is measured, and from this, the energy spectrum is determined. Finally, the results are compared to reference data.

\subsection{Setup}

\begin{wrapfigure}{L}{0.55\textwidth}
\centering
\includegraphics[width=9cm, height=5cm]{STF}
\caption{A schematic drawing of the \ac{STF}}
\label{fig:STF}
\end{wrapfigure}

A schematic \ac{3D} drawing of the \ac{STF} is shown in Fig.~\ref{fig:STF}. The interlocked area (A in Fig.) is the space where the irradiations are performed. The \ac{DAQ} area (B in Fig.) houses the electronics and other equipment. Data-acquisition is performed and experiments are controlled from here. Additional storage space for detectors and electronics as well as a staging area (C in Fig.) is also provided.

\begin{wrapfigure}{r}{0.28\textwidth}
\centering
\includegraphics[width=4.4cm, height=4.2cm]{Tank}
\caption{A 3D schematic of the Aquarium}
\label{Tank}
\end{wrapfigure}

A water-filled tank (see Fig.~\ref{Tank}), the Aquarium, is located in the interlocked area and is used to define beams from the mixed $\mathrm{n}/\gamma$ radiation field that is emitted by a source. The inner chamber houses the neutron source and up to 4 $\gamma$-ray detectors. The source is located in the center of the Aquarium and the detectors closely surround the source. The Aquarium is filled with ultra pure, de-ionized water. Four beam-ports allow for four simultaneous, independent experiments to be performed. Through the beam ports, the neutron flux is directed from the source to up to 4 neutron detectors.

The simplest \ac{TOF} setup using the Aquarium requires the use of one neutron detector placed in front of one beam port and one $\gamma$-ray detector for the detection of the coincident $\gamma$-rays. To quadruple the count rate, 4 $\gamma$-ray detectors are used. Together they cover a larger region of space (solid angle) where the $\gamma$-rays can be detected, and thus more events are recorded over the same period of time.

The specific detectors used in this lab are \ac{YAP} single-crystal scintillator detectors\footnote{The \ac{YAP} \ac{PMT}s
are operated at about $\SI{-800}{V}$} for $\gamma$-ray detection and a \ac{NE-213} liquid-scintillator
detector\footnote{The \ac{NE-213} \ac{PMT} is operated at about $\SI{-800}{V}$} to detect neutrons. \ac{NE-213}
s also sensitive to $\gamma$-rays. The power supply for the detectors is located in the \ac{DAQ} area where the
signals from the detectors are also processed. The analog signals from the detectors are converted into logic
signals using analog electronics modules setup and ultimately fed into a time-to-digital
converter (\ac{TDC}). One signal starts the time measurement, the other one stops it (see Fig.~\ref{TOF_TDC}).
In this way, events are timed and the results are passed on to a data-acquisition system.

\subsection{The principle of Time-of-Flight}

The \ac{TOF} is a method for determining the kinetic energy of a neutron by measuring the time it takes for it to travel between two points located a known distance apart. It is one of the methods employed to distinguish neutrons from $\gamma$-rays\footnote{All $\gamma$-rays travel with the speed of light.}, and to determine the neutron energy. Many variations have been developed since the origin of the technique in 1935, only 3 years after the discovery of the neutron.

\begin{wrapfigure}{L}{0.52\textwidth}
\centering
\includegraphics[width=8.5cm]{TOF}
\caption{A diagram of \ac{TOF} showing the location of the source as well as the neutron and the $\gamma$-ray detection time and distance from the source.}
\label{fig:TOF}
\end{wrapfigure}

When an actinide/Be neutron source is used, one of the reactions is of a particular importance, as it provides a way to ``tag'' the neutrons. The reaction is:
\begin{equation}
\alpha + ^{9}\textrm{Be} \rightarrow ^{12}\textrm{C*} + n + Q.
\label{actinide/Be}
\end{equation}

\noindent An $\alpha$-particle from the decay of the actinide reacts with a $^{9}\textrm{Be}$ nucleus producing $^{12}\textrm{C}$ in the first excited state and a fast neutron. $Q$ represents the energy difference of initial and final states. The excited $^{12}\textrm{C*}$ promptly de-excites emitting a $4.44$ MeV $\gamma$-ray. Subsequently, either a simultaneous $\textrm{n} - \gamma$ or a simultaneous $\gamma - \gamma$ emission from the source follows. $\gamma - \gamma$ events originate from the decay of the actinide, for example
\begin{equation}
^{241}\textrm{Am} \rightarrow ^{237}\textrm{Np} + \alpha + \gamma_1 ,
\end{equation}
where the daughter nucleus de-excites via the emission of a gamma (typically $~60\,\textrm{keV}$) combined with the gamma resulting from the process described by Eq.~\ref{actinide/Be}.

The time $T_0$ of a reaction resulting in a $\textrm{n} - \gamma$ emission is determined using the $\gamma$-ray. It travels with the speed of light, and is allocated at a certain time $T_{\gamma}$ a certain distance $s$ from the source. The neutron is allowed to travel to a point a greater distance $S$ away from the source. The time $T_n$ it takes for the neutron to reach that point is recorded. Thus, the velocity and energy of the neutron can be calculated (see Fig.~\ref{fig:TOF}).

\subsection{Optimization and calibration}

The prompt emission of a pair of $\gamma$-rays from the source can be used to  calibrate the \ac{TOF} \ac{TDC}. Since the \ac{YAP} detectors are closer to the source than the \ac{NE-213} detector, they are exposed to a much larger $\gamma$-ray flux coming from the source. \ac{TOF} measurements started with one of the \ac{YAP}s will rarely be completed simply because neither a $\gamma$-ray nor a neutron will be detected in the \ac{NE-213} detector due simply to geometry. If on the other hand the signal from the \ac{NE-213} detector is used to trigger an event, it is far more likely that a stop signal will be received from one of the \ac{YAP} detectors.

\begin{wrapfigure}{L}{0.45\textwidth}
\includegraphics[width=7.7cm, height=3.6cm]{TOF_TDC}
\caption{A basic diagram of \ac{TOF} measurement using a \ac{TDC}. The time measurement is started using the \ac{NE-213} detector and stopped by a delayed signal from the \ac{YAP} detector.}
\label{TOF_TDC}
\end{wrapfigure}

It is not possible for a $\gamma$-ray to take longer to travel from the source to the \ac{YAP} detector than for it to travel to the more distant \ac{NE-213} detector. The much slower neutron will take even more time to get to the \ac{NE-213} detector. To compensate, the signal from the \ac{YAP} detector is delayed (see Fig. \ref{TOF_TDC}).

The absolute \ac{TDC} values are thus influenced by the experimental setup. However, the time difference $\Delta \textit{t}_{\gamma,\mathrm{n}}$ between  a $\gamma - \gamma$ and a $\textit{n} - \gamma$ coincidence does not change. This fixed difference is used to calculate the neutron time-of-flight \textit{T}$_{\mathrm{n}}$:
\begin{equation}
\label{equ:TOF_neutron}
\mathit{T}_{\mathrm{n}} =\Delta \textit{t}_{\gamma,\mathrm{n}}+(\mathit{T}_{\gamma} - \mathit{T}_0).
\end{equation}
where $\mathit{T}_0$ is the start time of an event, and $\mathit{T}_{\gamma}$ is the time of a $\gamma- \gamma$ event.

\subsection{The neutron Time-of-Flight spectrum and Energy}

\begin{wrapfigure}{r}{0.65\textwidth}
\centering
\includegraphics[width=9.7cm, height=5cm]{TOF_spectrum}
\caption{A measured \ac{TDC} coincidence spectrum between particles detected in the \ac{NE-213} and \ac{YAP} detectors \cite{nilsson}.}
\label{TOF_spectrum}
\end{wrapfigure}

The \ac{TOF} spectrum of an actinide/Be source has a characteristic shape with two regions containing most events: one from $\textit{n} - \gamma$ and the other from $\gamma - \gamma$  coincidences (shown in Fig.~\ref{TOF_spectrum}).
In the first case, a neutron is detected in the \ac{NE-213} detector and a $\gamma$-ray is detected in the \ac{YAP} detector. The prompt $\textit{n} - \gamma$ distribution is wide and structured, since neutrons corresponding to the first-excited state of $^{12}$C are emitted with a wide range of energies, depending upon the emission angle, for example. Note that $\mathit{Q}$-value $\SI{4.44}{MeV}$ of the reaction $\mathit{Q}$-value are lost to the $\gamma$-ray used to tag the neutron.

In the second case, a narrow peak, known as the $\gamma$-flash, is produced by the detection of a $\gamma$-ray in the \ac{NE-213} detector and a correlated $\gamma$-ray in the \ac{YAP} detector. The reaction time \textit{T}$_{0}$ relative to the $\gamma$-flash can be obtained if the \ac{TDC} time resolution ($\SI{200}{ps/CH}$) is known. The time difference between $\mathit{T}_{\gamma}$ and $\mathit{T}_0$ is a function of the speed of light and the distance between the source and the YAP detectors.

Due to the delay of the \ac{YAP} signal, the neutron region populates lower \ac{TDC} channels, while the $\gamma$-flash is located at higher TDC channels.

\begin{wrapfigure}{L}{0.65\textwidth}
\centering
\includegraphics[width=10.3cm]{comparison}
\caption{The tagged-neutron spectrum obtained by Scherzinger et.al. (2015), marked with grey, together with ISO 8529-2 reference and Lorch (1973) spectra, tagged-neutron results obtained by Geiger and Hargrove (1964), as well as theoretical calculations by Vijaya and Kumar (1973), Van der Zwan (1968) and De Guarrini and Malaroda (1971) \cite{Scherzinger1}.}
\label{comparison}
\end{wrapfigure}

The neutron time-of-flight can be transformed into the kinetic energy $E_{k_n}$ according to:
\begin{equation}
\label{equ:E_{k_n}}
E_{k_n} =\frac{1}{2} m_n \frac{s^2}{T_n^2 c^2},
\end{equation}
where $\mathit{T}_n$ is the neutron time-of-flight, $m_n$ is the neutron mass and $c$ is the speed of light. Using the classical energy formula is acceptable in this work due to the low neutron energies ($E_{k_n} < \SI{10}{MeV}$) involved.

The obtained neutron energy spectrum may then be compared to reference data. The efficiency of the apparatus as well as the equipment resolution and precision may be analyzed. An example of a comparison is shown in Fig.~\ref{comparison}.

\pagebreak
