#!/usr/bin/env bash
echo "Zipping up files for FAFF11:"
7z a FAFF11_tof_lab_jupyter_notebook.zip data histhelpers.py figures/ neutron-lab_notebook_faff11.ipynb fithelpers.py
